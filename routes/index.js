var express = require('express');
var router = express.Router();
const usuariosController = require('../controllers').usuarios;
const rolesController = require('../controllers').roles;

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'API Sistema de Información Educativa' });
});

router.get('/api/roles', rolesController.list);
router.get('/api/roles/:id', rolesController.getById);
router.post('/api/roles', rolesController.add);
router.put('/api/roles/:id', rolesController.update);
router.delete('/api/roles/:id', rolesController.delete);
router.get('/api/active/roles', rolesController.active);

router.get('/api/usuarios', usuariosController.list);
router.get('/api/usuarios/:id', usuariosController.getById);
router.post('/api/usuarios', usuariosController.add);
router.put('/api/usuarios/:id', usuariosController.update);
router.delete('/api/usuarios/:id', usuariosController.delete);

module.exports = router;
